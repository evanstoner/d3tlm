var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var amqp = require('amqplib/callback_api');

const MQ_IP = process.env.MQ_IP || 'rabbitmq';

// serve static content
app.use(express.static('public'));

// listen for message queue updates
function amqpConnect() {
  amqp.connect('amqp://' + MQ_IP, function(err, conn) {
    if (err) {
      console.log(err);
      setTimeout(amqpConnect, 3000);
      return;
    }

    console.log('mq connected');

    conn.createChannel(function(err, ch) {
      console.log('channel created');
      var q = 'symbols';

      ch.assertQueue(q, {durable: false});

      ch.consume(q, function(msg) {
        console.log(msg.content.toString());
        io.emit('symbol', JSON.parse(msg.content.toString()));
      }, { noAck: true });
    });
  });
}
amqpConnect();

// listen for websocket clients
io.on('connection', function(socket){
  console.log('a user connected');
});

// start the webserver
http.listen(3000, function(){
  console.log('listening on *:3000');
});
