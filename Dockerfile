FROM node:8-alpine

WORKDIR /d3tlm

COPY package*.json ./

RUN npm install

COPY . .

CMD ["node", "index.js"]

EXPOSE 3000
