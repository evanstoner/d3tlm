var colors;
var socket;
var svgWidth;
var svgHeight;

/* uncomment to simulate symbol updates locally */
// const NUM_SYMBOLS = 4000;
// const UPDATE_INTERVAL = 100;
// setInterval(function() {
//   for (var i = 0; i < NUM_SYMBOLS; i++) {
//     onSymbol({
//       "symbol": "" + i,
//       "value": Math.random()
//     });
//   }
// }, UPDATE_INTERVAL);

$(function() {
  // resize the screen, and listen for future resizes
  onWindowResize();
  $('#crt').height(svgHeight);
  $(window).resize(onWindowResize);

  // init color scheme
  colors = d3.scaleSequential(d3.interpolateViridis);

  // open incoming websockets
  socket = io();
  socket.on('symbol', onSymbol);
});

function onWindowResize() {
  svgWidth = $('#crt').width();
  svgHeight = svgWidth * 2/3;
}

const SYM_WIDTH = 10;
const SYM_HEIGHT = 10;
const SYM_PAD = 2;

function onSymbol(data) {
  let crt = d3.select('#crt');
  let symbol = crt.selectAll('#symbol_' + data.symbol)
    .data([data])
    .style('fill', function(d) { return colors(d.value); })
    .attr('x', x)
    .attr('y', y)
    .enter()
      .append('rect')
      .attr('id', 'symbol_' + data.symbol)
      .attr('x', x)
      .attr('y', y)
      .attr('width', SYM_WIDTH)
      .attr('height', SYM_HEIGHT)
      .style('fill', function(d) { return colors(d.value); })
    ;
}

function x(d) {
  let index = Number.parseInt(d.symbol);
  return index * (SYM_WIDTH + SYM_PAD) % svgWidth;
}

function y(d) {
  let index = Number.parseInt(d.symbol);
  return Math.floor(index * (SYM_HEIGHT + SYM_PAD) / svgWidth) * (SYM_HEIGHT + SYM_PAD);
}
